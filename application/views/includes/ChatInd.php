

<!-- <!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head> -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">
  <title></title>
  <link rel="apple-touch-icon" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/css/le-style.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/waves/waves.css">
  <!--<link rel="stylesheet" href="vendor/fullcalendar/fullcalendar.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/bootstrap-touchspin.css">-->
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/jquery-selective/jquery-selective.css">
  <!--<link rel="stylesheet" href="examples/css/apps/calendar.css">-->
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/alertify/alertify.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/notie/notie.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/examples/css/advanced/alertify.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/jquery-wizard/jquery-wizard.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/core/steps/jquery.steps.css">

  <!-- Bitsclan code here -->
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/fullcalendar/fullcalendar.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/examples/css/apps/calendar.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/examples/css/uikit/modals.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" type="text/css" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/datetimepicker/build/jquery.datetimepicker.min.css">
      <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
  
  <!-- Bitsclan code end here -->
  <style>
  /*form .error {
    width: 100%;
    margin-top: .286rem;
    font-size: 100%;
    color: #f44336;
    }

    form .form-control.error {
    border: #f44336 2px solid;
    }*/

    .rTable {
      display: table;
      width: 100%;
    }

    .rTableRow {
      display: table-row;
    }

    .rTableBody {
      display: table-row-group;
    }

    .rTableCell, .rTableHead {
      display: table-cell;
      padding: 3px 10px;
      border: 1px solid #999999;
    }

    .step.active ..tab-step-number, .step.current .tab-step-number {
      color: #3f51b5;
      background-color: #fff;
    }
    .dataTables_wrapper{
      width: 100% !important;
    }
    .dataTables_paginate{
      float: right;
    }
    /*.tab-step-number {
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        position: absolute;
        top: 50%;
        left: 5px;
        width: 20px;
        height: 20px;
        font-size: 15px;
        line-height: 20px;
        color: #fff;
        text-align: center;
        background: #e0e0e0;
        border-radius: 50%;
    }

    .step-desc {
        text-align: left;
    }

    .steps-sm .step-number~.step-desc {
        min-height: 30px;
        margin-left: 40px;
    }*/

    #server-data_filter.dataTables_filter {
      float: right;
    }

  </style>

  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/summernote/summernote.css">
  <link rel="stylesheet" type="text/css" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/emoji/dist/emojionearea.min.css" media="screen">

  <!-- Fonts -->
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/monokai-sublime.min.css" />
  <link rel="stylesheet" href="https://cdn.quilljs.com/1.3.6/quill.snow.css" />



    <!--[if lt IE 9]>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/html5shiv/html5shiv.min.js"></script>

  <![endif]-->



    <!--[if lt IE 10]>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/media-match/media.match.min.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/respond/respond.min.js"></script>

  <![endif]-->



  <!-- Scripts -->

  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/breakpoints/breakpoints.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/jquery/jquery.js"></script>

  <!-- bitsclan code here -->

  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/popper-js/umd/popper.min.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootstrap/bootstrap.js"></script>  
  <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
  <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/animsition/animsition.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/waves/waves.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCS-DB39Kk-Z25C5GWymVGshXIALbjXPGY&libraries=places"></script>

    <!-- bitsclan code end here -->

  <!--<script src="js/Plugin/jquery-wizard.js"></script>-->

  <!--<script src="examples/js/forms/wizard.js"></script>-->

  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/core/notie/notie.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/core/uiblocker/js/jquery.blockUI.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/core/js/core.js"></script>
  <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/core/steps/jquery.steps.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script>
    Breakpoints();
      function toasterOptions() {
           toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-bottom-right",
              "preventDuplicates": false,
              "onclick": null,
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }

        };
        
        toasterOptions();
  </script>

  <style>

    /*Bitsclan Code here*/
    .app-message .page-main{
      height: 100% !important;
      background-color: #fff !important;
    }
    #calendar-list .tooltiptext{
      display:none!important;
    }
    .error{
      color: red;
    }
    .dis-grid{
      display: grid;
    }

    .fc-h-event {
      border: 1px solid #3788d8 !important;
      border: 1px solid #3788d8 !important;
      background-color: #3788d8 !important;
    }

    .fc-daygrid-dot-event {
      background-color: transparent !important; 
      border-color: transparent !important; 
    }

  </style>

<!-- </head> -->
<body>

   
  <script type="text/javascript">
    function showMenuSubmenu(id){
      $('#'+id).children().css('visibility', 'visible');
      $('#'+id).children().css('opacity', 1)
    }
    $('#communication-drop-down').click(function(){
      console.log('i am clicked');
      $(this).find('ul').children().css('visibility', 'visible');
      $(this).find('ul').children().css('opacity', 1)
      
    });
    $('.reporting-drop-down').click(function(){
      console.log('i am clicked');
      $(this).find('ul').children().css('visibility', 'visible');
      $(this).find('ul').children().css('opacity', 1)
      
    });

  </script>

<link rel="stylesheet" href="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/examples/css/apps/message.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.scrollable-container{
  height: 760px !important;
}
.userssData{
  display: none;
}
.wid100{
    width:100%!important;
}
.wid93{
    width:93%!important;
    border:1px solid #d3d3d3!important;
}
.wid3{
    width:3%!important;
}
.papericon{
    width:22px!important;
}
.camera_border{
    border:1px solid #d3d3d3!important;
}
.emojionearea{
    border: none!important;
box-shadow: none!important;
background-image: none!important;
}
.app-message-chats .chats .chat-avatar .avatar{
  width: 40px!important;
  margin-top: 0px!important;
}
.custom-height.scrollable-container{
  height: 480px!important;
}
@media(max-width:576px){
    .wid93{
        width:78%!important;   
    }
    .wid3{
        width:10%!important;
    }
}
.wid100{
  width: 100%!important;
}
.wid70{
  width: 70%!important;
}
.wid30{
  width: 30%!important;
  float: right;
}

.attach-file{

    border-radius: 10px;
}

.middle {
 transition: .5s ease;
z-index: 9999;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
    cursor: pointer;
    color: red;
}
.rel{
  position: relative;
      float: left;
      overflow: hidden;
        margin-top: 10px;
    margin-bottom: 10px;

}
.opacity-s {
    background-color: #00000094;
    height: 100%;
    float: left;
    z-index: 999;
    position: absolute;
    width: 100%;
    border-radius: 10px;
}
</style>
<div class="page">

  <!-- Message Sidebar -->
  <div class="page-aside">
    <div class="page-aside-switch">
      <i class="icon md-chevron-left" aria-hidden="true"></i>
      <i class="icon md-chevron-right" aria-hidden="true"></i>
    </div>
    <div class="page-aside-inner">
      <div class="app-message-list page-aside-scroll">
        <div data-role="container">
          <div data-role="content" id="content">
            <ul class="list-group" id="admin-list"></ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Message Sidebar -->
  <div class="page-main">


 
    <!-- Chat Box -->
    <div class="app-message-chats">
      <button type="button" id="current-inbox-title" class="btn btn-round btn-default btn-flat primary-500">History Messages</button>
       <div class="page-aside-scroll scrollable is-enabled scrollable-vertical is-hovering" style="position: relative;">
        <div data-role="container" class="custom-height scrollable-container">
          <div data-role="content" id="content" class="scrollable-content">
      <div class="chats" id="messages-list" style="min-height:650px;"></div>
      </div>
</div>
</div>
    </div>
    <!-- End Chat Box -->

    <!-- Message Input-->
      <form class="app-message-input" id="send-message-form" method="POST" name="message-form" action="https://dev.247staffingsolutions.com.au/portal/index.php/administrator/inbox/send" enctype="multipart/form-data" novalidate>
      <input type="hidden" name="inboxInteractionId" id="messages-id">
      <input type="hidden" name="type" id="message-type">
      <input type="hidden" name="receiver" id="receiver">
      <div class="input-group form-material wid100">
        <span class="input-group-prepend wid3">
          <input type="file" id="uploadImage" name="attachment" class="attachment-input btn btn-pure btn-default icon md-camera camera_border" onchange="PreviewImage();">
        </span>
        <span class="wid93">
        <input class="form-control" type="text" id="message-input" placeholder="Type message here ...">
        </span>
        <div id="container"></div>
        <span class="input-group-append wid3">
          <button type="submit" class="btn btn-pure btn-default icon md-mail-send camera_border" disabled=""></button>
        </span>
      </div>
      <div class="overlay-container" style="display: none;">
        <span class="rel">
          <div class="opacity-s"></div>
        <img id="uploadPreview"  class="attach-file" width="90px" height="60px" />
     
          <i class="middle icon md-delete" style="color: #ff2525;" onclick="clearinput();"></i>
       </span>
      </div>
    </form>
    <br>
  </div>
</div>
  
<script type="text/javascript" src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>



  

   

  
    <!-- Core  -->

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>

    

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/popper-js/umd/popper.min.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootstrap/bootstrap.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/animsition/animsition.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/waves/waves.js"></script>

    

    <!-- Plugins -->

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/switchery/switchery.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/intro-js/intro.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/screenfull/screenfull.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/jquery-ui/jquery-ui.min.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/moment/moment.min.js"></script>



        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/jquery-selective/jquery-selective.min.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/bootbox/bootbox.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/vendor/autosize/autosize.js"></script>



    

    <!-- Scripts -->

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Component.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Base.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Config.js"></script>

    

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/js/Section/Menubar.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/js/Section/Sidebar.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/js/Section/PageAside.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/js/Plugin/menu.js"></script>

    

    <!-- Config -->



    <!-- Page -->

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/template/assets/js/Site.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/asscrollable.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/slidepanel.js"></script>

    <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/switchery.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/bootstrap-touchspin.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/bootstrap-datepicker.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/material.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/action-btn.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/editlist.js"></script>

        <script src="https://dev.247staffingsolutions.com.au/portal/icon_bar_assets/global/js/Plugin/bootbox.js"></script>



    

        

  </body>

</html>