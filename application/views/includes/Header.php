<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from coderthemes.com/moltran/layouts/red-vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 06 Mar 2021 10:28:20 GMT -->
<head>
        <meta charset="utf-8" />
        <title>Dashboard | Moltran - Responsive Bootstrap 4 Admin Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Responsive bootstrap 4 admin template" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

        <!-- Plugins css-->
        <link href="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400&display=swap" rel="stylesheet">
        <style>
            .card-box{
                color:black;
            background-color:#D2B48C;
            }
            .navbar-custom .app-search .btn {
    background-color: #574D44;
    }

    .card, .card-box {
        color:black;
        background-color:#D2B48C;
    }
    #sidebar-menu>ul>li>a.active {
    color:;
    /* background: #D2B48C; */
    background-color: #322D2A;
    
}
.font-side{

    font-weight: bolder; 
    height:20px;
    width:50px;
   border:30px;
}
.sidebar-s{

    color: black;
    font-size:15px;
    
}
p{

    font-family: 'Roboto Condensed', sans-serif;
}
#side-menu li:hover a{
    color: #000000;
}
        </style>
    </head>

    <body style="background-color: #483C32;">

        <!-- Begin page -->
        <div id="wrapper">

            
            <!-- Topbar Start -->
            <div  style="background-color: #322D2A;" class="navbar-custom">
                <ul class="list-unstyled topnav-menu float-right mb-0">

         
                    <li class="dropdown notification-list d-none d-md-inline-block">
                        <a href="<?php echo base_url(); ?>#" id="btn-fullscreen" class="nav-link waves-effect waves-light">
                            <i class="mdi mdi-crop-free noti-icon"></i>
                        </a>
                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="<?php echo base_url(); ?>#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="mdi mdi-bell noti-icon"></i>
                            <span class="badge badge-danger rounded-circle noti-icon-badge">3</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5 class="font-16 m-0">
                                    <span class="float-right">
                                        <a href="<?php echo base_url(); ?>#" class="text-dark">
                                            <small>Clear All</small>
                                        </a>
                                    </span>Notification
                                </h5>
                            </div>

                            <div class="slimscroll noti-scroll">
                    
                                <!-- item-->
                                <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon">
                                        <i class="fa fa-user-plus text-info"></i>
                                    </div>
                                    <p class="notify-details">New user registered
                                        <small class="noti-time">You have 10 unread messages</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon text-success">
                                        <i class="far fa-gem text-primary"></i>
                                    </div>
                                    <p class="notify-details">New settings
                                        <small class="noti-time">There are new settings available</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon text-danger">
                                        <i class="far fa-bell text-danger"></i>
                                    </div>
                                    <p class="notify-details">Updates
                                        <small class="noti-time">There are 2 new updates available</small>
                                    </p>
                                </a>
                            </div>

                            <!-- All-->
                            <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item text-center notify-item notify-all">
                                    See all notifications
                            </a>

                        </div>
                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="<?php echo base_url(); ?>#" role="button" aria-haspopup="false" aria-expanded="false">
                      
                            <img src="<?php echo base_url(); ?>assets/images/24.ico" alt="user-image" class="rounded-circle">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- item-->
                            <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-face-profile"></i>
                                <span>Profile</span>
                            </a>

                            <!-- item-->
                            <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-settings"></i>
                                <span>Settings</span>
                            </a>

                            <!-- item-->
                            <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-lock"></i>
                                <span>Lock Screen</span>
                            </a>

                            <div class="dropdown-divider"></div>

                            <!-- item-->
                            <a href="<?php echo base_url(); ?>javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-power-settings"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>

                    <li class="dropdown notification-list">
                        <a href="<?php echo base_url(); ?>javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                            <i class="mdi mdi-settings-outline noti-icon"></i>
                        </a>
                    </li>


                </ul>

                <!-- LOGO -->
                <div class="logo-box">
                        <a href="<?php echo base_url(); ?>index.html" style="margin-left:px;margin-top:14px;" class="logo  text-center logo">
                            <span class="logo-lg">
                                <img src="<?php echo base_url(); ?>assets\images/24.ico" alt="" height="100">
                                <!-- <span class="logo-lg-text-dark">Moltran</span> -->
                            </span>
                            <span class="logo-sm">
                                <!-- <span class="logo-lg-text-dark">24*7</span> -->
                           <img src="<?php echo base_url(); ?>assets\images/24.ico" alt="" height="40">
                            </span>
                        </a>

                        <a href="<?php echo base_url(); ?>index.html" class="logo text-center logo-light">
                            <!-- <span class="logo-lg">
                                <img src="<?php echo base_url(); ?>assets\images/24.ico" alt="" height="16">
                                 <span class="logo-lg-text-dark">24*7</span>
                            </span> -->
                            <!-- <span class="logo-sm">
                                <span class="logo-lg-text-dark">M</span>
                                <img src="<?php echo base_url(); ?>assets/images/logo-sm.png" alt="" height="25"> -->
                            </span> 
                        </a> 
                    </div>

                <!-- LOGO -->
  

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button class="button-menu-mobile waves-effect waves-light">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
        
                    <li class="d-none d-sm-block">
                        <form class="app-search">
                            <div class="app-search-box">
                                <div class="input-group">
                                    <input style="background-color: #574D44;" type="text" class="form-control" placeholder="Search...">
                                    <div class="input-group-append">
                                        <button style="color: #f4cc8f;" class="btn" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <!-- end Topbar -->
            
            <!-- ========== Left Sidebar Start ========== -->
  
            <div  style="background-color: #322D2A;" class="left-side-menu">

                    <div class="slimscroll-menu">
    
                        <!--- Sidemenu -->
                        <div id="sidebar-menu">
    
                            <div class="user-box">
                    
                                <!-- <div class="float-left">
                                    <img src="<?php echo base_url(); ?>assets/images/24.ico" alt="" class="avatar-md rounded-circle">
                                </div> -->
                                <div class="user-info">
                                    <!-- <div class="dropdown">
                                        <a href="<?php echo base_url(); ?>#" class="dropdown-toggle" style="color:#F3D89F" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                24*7 <i class="mdi mdi-chevron-down"></i>
                                                        </a>
                                        <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 29px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <li><a href="<?php echo base_url(); ?>javascript:void(0)" class="dropdown-item"><i class="mdi mdi-face-profile mr-2"></i> Profile<div class="ripple-wrapper"></div></a></li>

                                            <li><a href="<?php echo base_url(); ?>javascript:void(0)" class="dropdown-item"><i class="mdi mdi-power-settings mr-2"></i> Logout</a></li>
                                        </ul>
                                    </div> -->
                                    <!-- <p class="font-13 text-muted m-0">Administrator</p> -->
                                </div>
                            </div>
    
                            <ul class="metismenu" id="">
    
                               
                                <li onmouseover="this.style.background='#D2B48C';" onmouseout="this.style.background='#322D2A';" >
                                    <a href="<?php echo base_url(); ?>/Chat" class="waves-effect">
                                       <span  >
                                        <i class="mdi mdi-message-processing-outline"></i>
                                        <span  class="sidebar-s"><b>   Communication   </b> </span>
                                        </span>
                                    </a>
                                </li>
                                



                                <li  onmouseover="this.style.background='#D2B48C';" onmouseout="this.style.background='#322D2A';" onclick ="this.style.background='#322D2A';" class="active">
                   
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed waves-effect">
                    <i class="mdi mdi-message-processing-outline"></i>

                    <span  class="sidebar-s"><b>   Tutorial   </b> </span>

                    </a>
                   
                    <ul class="list-unstyled collapse" id="homeSubmenu" style="">
                    <li onmouseover="this.style.background='#D2B48C';" onmouseout="this.style.background='#322D2A';" >
                                    <a href="<?php echo base_url(); ?>Announcement" class="waves-effect">
                                       <span  >
                                       <i class=" mdi mdi-notebook"></i>
                                        <span  class="sidebar-s"><b>   Announcement   </b> </span>
                                        </span>
                                    </a>
                                </li>
                                
                                <li onmouseover="this.style.background='#D2B48C';" onmouseout="this.style.background='#322D2A';" >
                                    <a href="<?php echo base_url(); ?>" class="waves-effect">
                                       <span  >
                                       <i class=" mdi mdi-notebook"></i>
                                        <span  class="sidebar-s"><b>   Induction   </b> </span>
                                        </span>
                                    </a>
                                </li>
                                
                        
                    </ul>
                </li>






                               
                           
                                <li onmouseover="this.style.background='#D2B48C';" onmouseout="this.style.background='#322D2A';" >
                                    <a href="<?php echo base_url(); ?>About" class="waves-effect">
                                       <span  >
                                       <i class=" mdi mdi-notebook"></i>
                                        <span  class="sidebar-s"><b>   About   </b> </span>
                                        </span>
                                    </a>
                                </li>
                                
                                <li onmouseover="this.style.background='#D2B48C';" onmouseout="this.style.background='#322D2A';" >
                                    <a href="<?php echo base_url(); ?>About" class="waves-effect">
                                       <span  >
                                       <i class=" mdi mdi-notebook"></i>
                                        <span  class="sidebar-s"><b>   About company   </b> </span>
                                        </span>
                                    </a>
                                </li>
                
                           
                       
     </ul>
                      
    
                        </div>
                        <!-- End Sidebar -->
    
                        <div class="clearfix"></div>
    
                    </div>
                    <!-- Sidebar -left -->
    
                </div>