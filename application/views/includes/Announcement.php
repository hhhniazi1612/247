
            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

<button type="button" class="btn btn-info btn-xs" onclick="announce()">Click me</button>

<script>
function announce(){
Swal.fire({
  title: 'Title',
  text: `Within our organisation, we aim to maintain the highest standard of professionalism and are a member of the Australian Security Industry Association Limited (ASIAL).

We offer all staff on-going training programs to ensure that they are always informed of the latest developments in the security industry.

At 24 x7 Service Group we are dedicated to ensuring that all staff are not only licenced, but trained in professional distinction and customer service.`,
 
  imageUrl: '<?php echo base_url(); ?>assets/images/24.ico',
  imageWidth: 100,
  imageHeight: 100,
  imageAlt: 'Custom image',
})
}
</script>

</div>
</div>
</div>