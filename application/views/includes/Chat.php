                <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                    
                        <!-- end page title -->

                        <!--Widget-4 -->
                   
                        <!-- End row -->

                        <div class="row">
                            <!-- INBOX -->
                            <div style="margin-left:-40px" class="col-xl-4">
                                <div style="background-color:white;" class="card-body">
                                    <div class="card-header">
                                        <h3 class="card-title">Inbox</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="inbox-widget slimscroll" style="min-height: 384px;">
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Chadengle</p>
                                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                                    <p class="inbox-item-date">13:40 PM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-2.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Tomaslau</p>
                                                    <p class="inbox-item-text">I've finished it! See you so...</p>
                                                    <p class="inbox-item-date">13:34 PM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-3.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Stillnotdavid</p>
                                                    <p class="inbox-item-text">This theme is awesome!</p>
                                                    <p class="inbox-item-date">13:17 PM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-4.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Kurafire</p>
                                                    <p class="inbox-item-text">Nice to meet you</p>
                                                    <p class="inbox-item-date">12:20 PM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-5.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Shahedk</p>
                                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                                    <p class="inbox-item-date">10:15 AM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-6.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Adhamdannaway</p>
                                                    <p class="inbox-item-text">This theme is awesome!</p>
                                                    <p class="inbox-item-date">9:56 AM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-8.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Arashasghari</p>
                                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                                    <p class="inbox-item-date">10:15 AM</p>
                                                </div>
                                            </a>
                                            <a href="<?php echo base_url(); ?>#">
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-9.jpg" class="rounded-circle" alt=""></div>
                                                    <p class="inbox-item-author">Joshaustin</p>
                                                    <p class="inbox-item-text">I've finished it! See you so...</p>
                                                    <p class="inbox-item-date">9:56 AM</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end col -->

                            <!-- CHAT -->
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Chat</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="chat-conversation">
                                            <ul class="conversation-list slimscroll" style="min-height: 330px;">
                                                <li class="clearfix">
                                                    <div class="chat-avatar">
                                                        <img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" alt="male">
                                                        <i>10:00</i>
                                                    </div>
                                                    <div class="conversation-text">
                                                        <div class="ctext-wrap">
                                                            <i>John Deo</i>
                                                            <p>
                                                                Hello!
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="clearfix odd">
                                                    <div class="chat-avatar">
                                                        <img src="<?php echo base_url(); ?>assets/images/users/avatar-5.jpg" alt="Female">
                                                        <i>10:01</i>
                                                    </div>
                                                    <div class="conversation-text">
                                                        <div class="ctext-wrap">
                                                            <i>Smith</i>
                                                            <p>
                                                                Hi, How are you? What about our next meeting?
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="chat-avatar">
                                                        <img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" alt="male">
                                                        <i>10:01</i>
                                                    </div>
                                                    <div class="conversation-text">
                                                        <div class="ctext-wrap">
                                                            <i>John Deo</i>
                                                            <p>
                                                                Yeah everything is fine
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="clearfix odd">
                                                    <div class="chat-avatar">
                                                        <img src="<?php echo base_url(); ?>assets/images/users/avatar-5.jpg" alt="male">
                                                        <i>10:02</i>
                                                    </div>
                                                    <div class="conversation-text">
                                                        <div class="ctext-wrap">
                                                            <i>Smith</i>
                                                            <p>
                                                                Wow that's great
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-sm-9 chat-inputbar">
                                                    <input type="text" class="form-control chat-input" placeholder="Enter your text">
                                                </div>
                                                <div class="col-sm-3 chat-send">
                                                    <button type="submit" class="btn btn-info btn-block waves-effect waves-light">Send</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end col-->

                            <!-- TODOs -->
                      
                            <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container-fluid -->

                </div>
                <!-- end content -->

                

       